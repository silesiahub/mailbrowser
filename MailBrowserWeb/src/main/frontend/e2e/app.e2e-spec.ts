import { MailBrowserPage } from './app.po';

describe('mail-browser App', () => {
  let page: MailBrowserPage;

  beforeEach(() => {
    page = new MailBrowserPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
