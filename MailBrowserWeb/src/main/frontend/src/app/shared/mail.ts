export class Mail {
  from: String;
  to: String;
  title: String;
  content: String;
  sentDate: String
}
