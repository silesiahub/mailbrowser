export class PstEmail {
  title: string;
  messageBody: string;
  nodeId: number;
  numberOfAttachments: number;
  from: string;
  to: string;
  date: string;
  attachments: Array<string>;

  constructor() {
  }
}
