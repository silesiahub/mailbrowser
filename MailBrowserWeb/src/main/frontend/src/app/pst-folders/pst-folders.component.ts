import {MailServiceService} from "../mail-service.service";
import {PstFolder} from "./PstFolder";
import {PstEmailSimple} from "./PstEmailSimple";
import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: 'app-pst-folders',
  templateUrl: './pst-folders.component.html',
  styleUrls: ['./pst-folders.component.css']
})
export class PstFoldersComponent implements OnInit {
  @Input() subfolders: PstFolder[];
  emails: PstEmailSimple[];
  showCover = false;
    
  constructor(private mailService: MailServiceService) {
  }

  ngOnInit() {
  }

  listEmails(mailFolder) {
    this.showCover = true;
    this.mailService.loadEmails(mailFolder)
      .subscribe(
        data => {
          this.mailService.setPstEmails(data);
          this.emails = data;
            this.showCover = false;
        }
      );
  }

}
