import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {PstFoldersComponent} from "./pst-folders.component";

describe('PstFoldersComponent', () => {
  let component: PstFoldersComponent;
  let fixture: ComponentFixture<PstFoldersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PstFoldersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PstFoldersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
