export class PstFolder {
  nodeId: number;
  name: string;
  subfolders: PstFolder[];
  hasEmails: boolean;
  contentCount: number;

  constructor() {
  }
}
