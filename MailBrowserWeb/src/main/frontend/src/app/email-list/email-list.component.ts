import {MailServiceService} from "../mail-service.service";
import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {PstEmail} from "../pst-folders/PstEmail";
import {PstEmailSimple} from "../pst-folders/PstEmailSimple";
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-email-list',
  templateUrl: './email-list.component.html',
  styleUrls: ['./email-list.component.css']
//  providers: [MailServiceService]
})
export class EmailListComponent implements OnInit {
//  @Input() emails: PstEmail[];
  _emails: PstEmailSimple[];

  @Input()
  set emails(emails: PstEmailSimple[]) {
    this._emails = emails;
    this.rows = emails;
    this.temp = emails;
  }

  get emails(): PstEmailSimple[] { return this._emails; }

  currentMessage: PstEmail;

  selected = [];

  rows = [];
  temp = [];

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private mailService: MailServiceService) {
  }

  ngOnInit() {
  }

  loadMessage = function (nodeId) {
    this.mailService.loadMessage(nodeId)
      .subscribe(
        data => this.currentMessage = data
      );
  };

   onSelect({ selected }) {
    this.loadMessage(this.selected[0].nodeId);
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.title.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
