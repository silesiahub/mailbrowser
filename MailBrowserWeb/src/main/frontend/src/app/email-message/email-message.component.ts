import {PstEmail} from "../pst-folders/PstEmail";
import {Component, Input, OnInit} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {Headers, Http, ResponseContentType} from "@angular/http";
import {UserService} from "../user.service";
import "rxjs/Rx";
import * as FileSaver from "file-saver";
@Component({
  selector: 'app-email-message',
  templateUrl: './email-message.component.html',
  styleUrls: ['./email-message.component.css']
})
export class EmailMessageComponent implements OnInit {
  @Input() message: PstEmail;

  constructor(private sanitizer: DomSanitizer, private userService: UserService, private _http: Http) {
  }

  ngOnInit() {
  }

  prepareHtml = function (html_code) {
    return this.sanitizer.bypassSecurityTrustHtml(html_code);
  };

  downloadAttachment(nodeId, i, filename) {
    let headers = new Headers();
    headers.append("Authorization", this.userService.getAuthenticationToken());
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("X-Requested-With", "XMLHttpRequest");
    this._http.get('http://52.166.120.118:8080/api/attachment/' + this.message.nodeId + '/' + i, {
      headers: headers,
      responseType: ResponseContentType.Blob
    }).subscribe(
      data => this.downloadFile(data, filename),
      err => alert("Error while downloading attachment!"),
      () => console.log('Request Complete')
    );
  }

  downloadFile(response, filename) {
    var blob = new Blob([response.blob()], {type: 'application/jpg'});
    FileSaver.saveAs(blob, filename);
  }

//  renderHtml = function (html_code) {
//    return $sce.trustAsHtml(html_code);
//  };
  range = function (max) {
    const step = 1;
    const input = [];
    for (let i = 1; i <= max; i += step) {
      input.push(i);
    }
    return input;
  };


  printToCart(printSectionId: string) {
    let popupWinindow;
    const innerContents = document.getElementById(printSectionId).innerHTML;
    popupWinindow =
      window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" />' +
      '</head><body onload="window.print()">' + innerContents + '</html>');
    popupWinindow.document.close();
  };

}
