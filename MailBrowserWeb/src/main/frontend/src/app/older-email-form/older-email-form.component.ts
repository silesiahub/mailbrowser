import {Component, OnInit} from "@angular/core";
import {Headers, Http} from "@angular/http";

@Component({
  selector: 'app-older-email-form',
  templateUrl: './older-email-form.component.html',
  styleUrls: ['./older-email-form.component.scss']
})
export class OlderEmailFormComponent implements OnInit {
  successMessage = "";
  requestSent: boolean = false;
  color = 'primary';
  mode = 'query';
  value = 50;
  bufferValue = 75;
  constructor(private _http: Http) {
    this.requestData = {
      "requestedMailAddress": "",
      "fromDate": "",
      "toDate": "",
      "additionalInformation": "",
      "userMail": "",
      "username": "",
      "userPhoneNumber": ""
    };
  }

  requestData = {};

  ngOnInit() {
  }

  onSubmit() {
    let headers = new Headers();
    headers.append("Authorization", sessionStorage.getItem("authenticationToken"));
    headers.append("Content-Type", "application/json");
    headers.append("X-Requested-With", "XMLHttpRequest");
    //start
    this.requestSent = true;
    this._http.post('api/archiveMail/', {
      "requestedMailAddress": this.requestData["requestedMailAddress"],
      "fromDate": this.requestData["fromDate"],
      "toDate": this.requestData["toDate"],
      "additionalInfo": this.requestData["additionalInformation"],
      "userMail": sessionStorage.getItem("userEmail"),
      "username": sessionStorage.getItem("user"),
      "userPhoneNumber": sessionStorage.getItem("userPhone")
    }, {
      headers: headers
    }).subscribe(
      data => this.onSuccess(),
      err => this.onError(),
      () => console.log('Mail should be sent')
    );
  }

  private onCancel() {
    this.requestData = {
      "requestedMailAddress": "",
      "fromDate": "",
      "toDate": "",
      "additionalInformation": "",
      "userMail": "",
      "username": "",
      "userPhoneNumber": ""
    };
  }
  private onSuccess() {
    this.successMessage = "Request was sent"
    this.requestSent = false;
  }

  private onError() {
    this.requestSent = false;
  }
}
