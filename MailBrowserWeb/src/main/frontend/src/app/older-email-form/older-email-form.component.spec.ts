import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OlderEmailFormComponent } from './older-email-form.component';

describe('OlderEmailFormComponent', () => {
  let component: OlderEmailFormComponent;
  let fixture: ComponentFixture<OlderEmailFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OlderEmailFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OlderEmailFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
