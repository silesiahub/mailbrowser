import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import {Router} from "@angular/router";

@Injectable()
export class UserService {

  constructor(private _http: Http, private _router: Router,) {
  }

  getAuthenticationToken() {
    return sessionStorage.getItem('authenticationToken');
  }

  logout() {
    let headers = new Headers();
    headers.append('Authorization', this.getAuthenticationToken());
    headers.append("X-Requested-With", "XMLHttpRequest");
    this._http.delete('http://52.166.120.118:8080/logout/', {
      headers: headers
    }).subscribe(
      data => this.logoutSuccess(),
      err => this.logoutErrorHandler(),
      () => console.log('Request Complete')
    );
    this._router.navigateByUrl("");
  }

  private logoutSuccess() {
    sessionStorage.removeItem('authenticationToken');
    sessionStorage.removeItem('user');
    this._router.navigate(['/']);
  }

  private logoutErrorHandler() {
    //  error
  }
}
