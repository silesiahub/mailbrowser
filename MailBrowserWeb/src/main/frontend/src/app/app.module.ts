import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppComponent} from "./app.component";
import {HttpModule} from "@angular/http";
import {PstFoldersComponent} from "./pst-folders/pst-folders.component";
import {EmailListComponent} from "./email-list/email-list.component";
import {EmailMessageComponent} from "./email-message/email-message.component";
import {MailServiceService} from "./mail-service.service";
import {MailBrowserModule} from "./mail-browser/mail-browser.module";
import {MdButtonModule, MdInputModule, MdDatepickerModule, MdNativeDateModule, MdCheckboxModule, MdProgressBarModule} from "@angular/material";
import {RouterModule, Routes} from "@angular/router";
import {EmailBrowserComponent} from "./email-browser/email-browser.component";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {MailboxesListComponent} from "./mailbox-browser/mailboxes-list/mailboxes-list.component";
import {LoginComponent} from "./login/login.component";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {UserService} from "./user.service";
import {OlderEmailFormComponent} from "./older-email-form/older-email-form.component";
import {FormsModule} from "@angular/forms";


const appRoutes: Routes = [
  {path: '', component: LoginComponent, pathMatch: 'full'},
  {path: 'Mailboxes', component: MailboxesListComponent, pathMatch: 'full'},
  {path: 'EmailBrowser', component: EmailBrowserComponent, pathMatch: 'full'},
  {path: 'ArchivedMail', component: OlderEmailFormComponent , pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    PstFoldersComponent,
    EmailListComponent,
    EmailMessageComponent,
    EmailBrowserComponent,
    MailboxesListComponent,
    LoginComponent,
    OlderEmailFormComponent
  ],
  imports: [
    MailBrowserModule,
    BrowserModule,
    HttpModule,
    MdButtonModule, // We need to import module from Material node_module when we want to use material control like grid
    NgxDatatableModule,
    MdNativeDateModule,
    MdDatepickerModule,
    RouterModule.forRoot(appRoutes, {useHash: true, enableTracing: true}),
    MdInputModule,
    BrowserAnimationsModule,
    MdCheckboxModule,
    MdProgressBarModule,
    FormsModule
  ],
  providers: [MailServiceService, UserService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
