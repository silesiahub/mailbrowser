import {PstEmail} from "./pst-folders/PstEmail";
import {PstEmailSimple} from "./pst-folders/PstEmailSimple";
import {PstFolder} from "./pst-folders/PstFolder";
import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";
import {UserService} from "./user.service";

@Injectable()
export class MailServiceService {
  private pstEmails: PstEmailSimple[];
  private fileName: string;

  constructor(private _http: Http, private userService: UserService) {
  }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Authorization', this.userService.getAuthenticationToken());
    headers.append("X-Requested-With", "XMLHttpRequest");
    return headers;
  }

  getFiles(): Observable<string[]> {
    return this._http.get('/api/structure', {
      headers: this.getHeaders()
    }).map(res => res.json().folders);
  }

  getPstStructure(file): Observable<PstFolder> {
    return this._http.get('/api/list/' + file + '/', {
      headers: this.getHeaders()
    })
      .map(res => res.json().pstFolder);
  }

  loadEmails(mailFolder): Observable<PstEmailSimple[]> {
    return this._http.get('/api/listMessages/' + mailFolder + '/', {
      headers: this.getHeaders()
    })
      .map(res => res.json().emails);
  }

  loadMessage(nodeId): Observable<PstEmail> {
    return this._http.get('/api/messageDetails/' + nodeId + '/', {
      headers: this.getHeaders()
    })
      .map(res => res.json().message);
  }

  getPstEmails(): PstEmailSimple[] {
    return this.pstEmails;
  }

  setPstEmails(emails) {
    this.pstEmails = emails;
  }

  getFileName(): string {
    return this.fileName;
  }

  setFileName(fileName) {
    this.fileName = fileName;
  }
}
