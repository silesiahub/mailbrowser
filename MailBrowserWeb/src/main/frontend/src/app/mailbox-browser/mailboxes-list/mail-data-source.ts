import {CollectionViewer, DataSource} from "@angular/cdk";
import {Observable} from "rxjs/Observable";
import {MailServiceService} from "../../mail-service.service";
import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class MailDataSource extends DataSource<string> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  constructor(public mailService: MailServiceService) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<string[]> {
    return this.mailService.getFiles();
  }

  disconnect(collectionViewer: CollectionViewer): void {
  }
}
