import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {MailServiceService} from "../../mail-service.service";
import {DatatableComponent} from "@swimlane/ngx-datatable/release";
import {Router} from "@angular/router";
import {Http} from "@angular/http";
import {UserService} from "../../user.service";

@Component({
  selector: 'app-mailboxes-list',
  templateUrl: './mailboxes-list.component.html',
  styleUrls: ['./mailboxes-list.component.scss']
})
export class MailboxesListComponent implements OnInit {
  constructor(private mailService: MailServiceService, private _router: Router, private _http: Http, private userService: UserService) {
  }

  columns = [
    {prop: 'mail'}
  ];

  private rows: any[] = [];
  private temp: any[] = [];
  private selected: any = [];
  dmgmedia = true;
  entertainment = true;
  cinemas = true;

  @ViewChild('filter')
  private filter: ElementRef;
  private username: String;
  @ViewChild(DatatableComponent) table: DatatableComponent;

  ngOnInit() {
    this.mailService.getFiles().subscribe(
      data => {
        data.forEach(file => {
          this.rows.push({mail: file})
          this.temp.push({mail: file})
        })
      }
    );
    this.username = sessionStorage.getItem('user');
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.temp.filter(function (row) {
      return row.mail.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
    this.filterCheckboxes();
  }

  onSelect(event) {
    console.log('Event: select', event, this.selected);
    this.mailService.setFileName(this.selected[0].mail);
    this._router.navigateByUrl('/EmailBrowser');
  }

  onActivate(event) {
    console.log('Event: activate', event);
  }

  logout() {
    this.userService.logout();
  }

  changeMedia() {
    this.dmgmedia = !this.dmgmedia;
    this.rows = this.temp;
    this.filterCheckboxes();
  }
  changeEntertainment() {
    this.entertainment = !this.entertainment;
    this.rows = this.temp;
    this.filterCheckboxes();

  }

  changeCinemas() {
    this.cinemas = !this.cinemas;
    this.rows = this.temp;
    this.filterCheckboxes();
  }

    filterCheckboxes(){
      if (!this.dmgmedia) {
          const temp = this.rows.filter(function (row) {
            return row.mail.toLowerCase().indexOf('dmgmedia.com') === -1;
          });
          this.rows = temp;
        }
      if (!this.entertainment) {
          const temp = this.rows.filter(function (row) {
            return row.mail.toLowerCase().indexOf('dmg-entertainment.com') === -1;
          });
          this.rows = temp;
        }
        if (!this.cinemas) {
          const temp = this.rows.filter(function (row) {
            return row.mail.toLowerCase().indexOf('dmg-cinemas.com') === -1;
          });
          this.rows = temp;
        }
      }

}
