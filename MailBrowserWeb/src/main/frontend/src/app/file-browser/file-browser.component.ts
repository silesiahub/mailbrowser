import { MailServiceService } from '../mail-service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-file-browser',
  templateUrl: './file-browser.component.html',
  styleUrls: ['./file-browser.component.css']
})
export class FileBrowserComponent implements OnInit {

  files: string[];

  constructor(private mailService: MailServiceService, private _router: Router) { }

  ngOnInit() {
    this.onLoadFiles();
  }

  onLoadFiles() {
    this.mailService.getFiles()
      .subscribe(
        data => {
          this.files = data;
        }
      );
  }

  getPstStructure(fileName) {
    this.mailService.setFileName(fileName);
    this._router.navigate(['/EmailBrowser']);
  }

}
