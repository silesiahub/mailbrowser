import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {FoldersStructureComponent} from "./folders-structure.component";

describe('FoldersStructureComponent', () => {
  let component: FoldersStructureComponent;
  let fixture: ComponentFixture<FoldersStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FoldersStructureComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoldersStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
