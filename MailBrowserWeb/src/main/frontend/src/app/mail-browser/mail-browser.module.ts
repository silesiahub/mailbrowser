import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FoldersStructureComponent} from "./folders-structure/folders-structure.component";
import {MailContentComponent} from "./mail-content/mail-content.component";
import {MailsListComponent} from "./mails-list/mails-list.component";


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FoldersStructureComponent,
    MailContentComponent,
    MailsListComponent]
})
export class MailBrowserModule {
}
