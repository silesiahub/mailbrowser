import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailBrowserComponent } from './email-browser.component';

describe('EmailBrowserComponent', () => {
  let component: EmailBrowserComponent;
  let fixture: ComponentFixture<EmailBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
