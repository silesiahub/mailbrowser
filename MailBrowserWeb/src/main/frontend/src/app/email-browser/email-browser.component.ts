import {MailServiceService} from "../mail-service.service";
import {PstFolder} from "../pst-folders/PstFolder";
import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'app-email-browser',
  templateUrl: './email-browser.component.html',
  styleUrls: ['./email-browser.component.css']
})
export class EmailBrowserComponent implements OnInit {
  folder: PstFolder;

  constructor(private mailService: MailServiceService, private _router: Router) {
  }

  ngOnInit() {
    this.mailService.getPstStructure(this.mailService.getFileName())
      .subscribe(
        data => {
          this.folder = data;
        },
        err => this.onBack()
      );
  }

  getFileName(): string {
    return this.mailService.getFileName().replace('.pst', '');
  }

  onBack(): void {
    this._router.navigate(['/Mailboxes']);
  }

}
