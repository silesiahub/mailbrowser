import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {Headers, Http} from "@angular/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _router: Router, private _http: Http) {
  }

  @ViewChild('login') login: ElementRef;
  @ViewChild('password') password: ElementRef;
  @ViewChild('sms') sms: ElementRef;
  public message: string = '';
  public smsError: string = '';
  private authenticationToken: string = '';
  private smsFormAvailable: boolean = false;

  ngOnInit() {
  }

  requestSmsCode() {
    let username = this.login.nativeElement.value;
    let pass = this.password.nativeElement.value;
    let headers = new Headers();
    this.authenticationToken = "Basic " + btoa(username + ":" + pass);
    headers.append("Authorization", this.authenticationToken);
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("X-Requested-With", "XMLHttpRequest");
      this._http.get('/login/', {
        headers: headers
      }).subscribe(
        data => this.requestSmsSuccess(data),
        err => this.loginError(),
        () => console.log('Request Complete')
      );
  }

  validateSmsCode() {
    let username = this.login.nativeElement.value;
    let pass = this.password.nativeElement.value;
    let smsCode = this.sms.nativeElement.value;
    let headers = new Headers();
    this.authenticationToken = "Basic " + btoa(username + ":" + pass);
    headers.append("Authorization", this.authenticationToken);
    headers.append("Content-Type", "application/json");
    headers.append("X-Requested-With", "XMLHttpRequest");
      this._http.post('/sms/', {
        "smsCode": smsCode
      }, {
        headers: headers
      }).subscribe(
        data => this.smsValidationSuccess(data),
        err => this.smsErrorHandler(),
        () => console.log('Request Complete')
      );
  }

  private requestSmsSuccess(data) {
    sessionStorage.setItem('authenticationToken', this.authenticationToken);
    sessionStorage.setItem('user', this.login.nativeElement.value);
    sessionStorage.setItem('userEmail', data.json()['email'])
    sessionStorage.setItem('userPhone', data.json()['userPhoneNumber'])
    this.smsFormAvailable = true;
  }

  private smsValidationSuccess(data) {
  this._router.navigateByUrl('/Mailboxes');
  }

  private loginError() {
    this.message = "Bad credentials";
  }

  private smsErrorHandler() {
    this.smsError = "Invalid sms code!";
  }
}
