sed -i -e"s/^max_connections = 100.*$/max_connections = 1000/" /var/lib/postgresql/data/postgresql.conf

psql < ./scripts/mailbrowser-db-create.sql
psql -d mailbrowser < ./scripts/mailbrowser-db-create-schema.sql