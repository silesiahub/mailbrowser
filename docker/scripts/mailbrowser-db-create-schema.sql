DROP SCHEMA IF EXISTS public;

--Resu
CREATE SCHEMA IF NOT EXISTS mailbrowser AUTHORIZATION mailbrowser;
GRANT ALL ON SCHEMA mailbrowser TO mailbrowser;
ALTER ROLE mailbrowser SET search_path = 'mailbrowser';