#MailBrowser


* MailBrowser for PST files
* Version [TODO]
* ReadMe created with Markdown: [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How to run ##
* Prepare database
1. Install docker [Docker](https://www.docker.com/get-docker)
2. Change directory to project's docker directory '/docker/'
3. Run:
      `docker-compose up` 
      
* Re-build project
1. Run `clean install` on root module (MailBrowser)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
