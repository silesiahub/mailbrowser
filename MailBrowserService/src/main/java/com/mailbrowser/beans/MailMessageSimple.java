package com.mailbrowser.beans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pff.PSTMessage;

import lombok.Data;

public @Data class MailMessageSimple {

	private String title;
	private long nodeId;
	private String date;
	
	public MailMessageSimple(PSTMessage pstMessage) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		this.title = pstMessage.getConversationTopic();
		this.nodeId = pstMessage.getDescriptorNodeId();
		this.date = sdf.format(pstMessage.getMessageDeliveryTime());
	}
	
}
