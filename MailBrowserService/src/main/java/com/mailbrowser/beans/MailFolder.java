package com.mailbrowser.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pff.PSTException;
import com.pff.PSTFolder;

import lombok.Data;
import lombok.Getter;

public @Data class MailFolder implements Serializable {
	private String name;
	private List<MailFolder> subfolders = new ArrayList<>();
	private boolean hasEmails;
	private int contentCount;
	private long nodeId;
	
	public MailFolder(PSTFolder pstFolder) {
		super();
		this.name = pstFolder.getDisplayName();
		this.hasEmails = pstFolder.getContentCount() > 0;
		try {
			this.contentCount = pstFolder.getEmailCount();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PSTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.nodeId = pstFolder.getDescriptorNodeId();
	}
	
	
}