package com.mailbrowser.beans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pff.PSTMessage;

import lombok.Data;

public @Data class MailMessage {

	private String title;
	private String messageBody;
	private String from;
	private String to;
	private long nodeId;
	private int numberOfAttachments;
	private String date;
	private List<String> attachments;
	
	public MailMessage(PSTMessage pstMessage, boolean details) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		this.title = pstMessage.getConversationTopic();
		this.messageBody = pstMessage.getBodyHTML();
		this.to = pstMessage.getDisplayTo();
		this.from = pstMessage.getSenderEmailAddress();
		this.nodeId = pstMessage.getDescriptorNodeId();
		this.numberOfAttachments = pstMessage.getNumberOfAttachments();
		this.date = sdf.format(pstMessage.getMessageDeliveryTime());
	}
}
