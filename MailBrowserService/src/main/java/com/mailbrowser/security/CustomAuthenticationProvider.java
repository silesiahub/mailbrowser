package com.mailbrowser.security;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

@Component
public class CustomAuthenticationProvider
    implements AuthenticationProvider {


    private @Autowired
    HttpServletRequest request;

    @Override
    public Authentication authenticate(Authentication authentication)
        throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        String smsCode = request.getParameter("smsCode");
        try {
            IOUtils.toString(request.getReader());
        } catch (IOException e) {
            return null;
        }
        if(request.getRequestURL().toString().contains("/login/sms")){
             if (false) {
                // use the credentials
                // and authenticate against the third-party system
                return new UsernamePasswordAuthenticationToken(
                    name, password, new ArrayList<>());
            } else {
                throw new BadCredentialsException("SMS CODE NOT OK");
            }
        } else {
            return new UsernamePasswordAuthenticationToken(
                name, password, new ArrayList<>());
        }

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
            UsernamePasswordAuthenticationToken.class);
    }
}
