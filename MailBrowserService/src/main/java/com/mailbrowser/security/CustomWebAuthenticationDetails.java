package com.mailbrowser.security;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

public class CustomWebAuthenticationDetails extends WebAuthenticationDetails {
    private String smsCode;

    public CustomWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        smsCode = request.getParameter("smsCode");
    }

    public String getSmsCode() {
        return smsCode;
    }
}