package com.mailbrowser;

import com.mailbrowser.domain.Role;
import com.mailbrowser.domain.RoleRepository;
import com.mailbrowser.domain.User;
import com.mailbrowser.domain.UserRepository;
import com.mailbrowser.domain.UserRole;
import com.mailbrowser.domain.UserRoleRepository;
import com.mailbrowser.service.LoginService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.EncoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RestController
@Slf4j
public class LoginController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private LoginService loginService;

    public static final String ACCOUNT_SID = "AC5f6af5a1d5592c63dc2cf95404ecebe0";
    public static final String AUTH_TOKEN = "8efd2b5370d9c80277f4a2d7a5f9de1c";


    private Integer generateSmsCode() {
        Random r = new Random(System.currentTimeMillis());
        return 10000 + r.nextInt(20000);
    }

//    private String encodeSmsCode(Integer smsCode) throws EncoderException {
//        return Base64.encodeInteger(BigInteger.valueOf(smsCode)).toString();
//    }
//
//    private String decodeSmsCode(String encodedSmsCode) {
//        return Base64.decodeInteger(encodedSmsCode.getBytes()).toString();
//    }

    public void removeRoleForUser(String username, Integer roleId) {//todo: move to some service
        userRoleRepository.findByUsername(username)
            .stream().filter(userRole -> userRole.getRoleId().equals(roleId))
            .forEach(userRoleRepository::delete);
    }

    public void removeAllAuthRolesForUser(String username) {
        Arrays.asList(roleRepository.findByRole("auth"), roleRepository.findByRole("preauth")).stream()
            .map(authRoles -> authRoles.getId())
            .forEach(authRoleId -> removeRoleForUser(username, authRoleId));
    }


    public void sendCodeToUser(String smsCode, String userNumber) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber(userNumber),
            new PhoneNumber("+17604725542"), "MailBrowser code is: " + smsCode).create();
        System.out.println(message.getSid());
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    //TODO: send sms code and save it to base// do not send OMG // hash the smsCode
    public Map<String, Object> login() throws EncoderException {
        Integer smsCode = generateSmsCode();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Role preAuthRole = roleRepository.findByRole("preauth");

        User user = userRepository.findByUsername(auth.getName())
            .orElseThrow(() -> new RuntimeException("User not found!"));
        sendCodeToUser(smsCode.toString(), user.getPhoneNumber());
        userRoleRepository.findByUsername(auth.getName())
            .stream().filter(userRole -> userRole.getRoleId().equals(preAuthRole.getId()))
            .forEach(userRoleRepository::delete);

        loginService.removeAllAuthRolesForUser(auth.getName());

        userRoleRepository.save(UserRole.builder()
            .roleId(preAuthRole.getId())
            .username(user.getUsername())
            .build());

        user.setSmsCode(smsCode.toString());

        //todo: time limited??!! it should be!
        //todo: serve the browser closing etc.! REMEMBER to logout the user
        userRepository.save(user);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("username", user.getUsername());
        model.put("email", user.getEmail());
        model.put("userPhoneNumber", user.getPhoneNumber());
        return model;
    }

    @RequestMapping(value = "/sms", method = RequestMethod.POST)
    @ResponseBody
    //TODO: send sms code and check with existing in base
    public HttpStatus sms(@RequestBody SmsCode smsCodeMessage) {
        String smsCode = smsCodeMessage.getSmsCode();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByUsername(auth.getName())
            .orElseThrow(() -> new RuntimeException("User not found!"));
        Role authRole = roleRepository.findByRole("auth");
        Role preAuthRole = roleRepository.findByRole("preauth");
        if (user.getSmsCode().equals(smsCode)) {
            removeRoleForUser(auth.getName(), preAuthRole.getId());
            removeRoleForUser(auth.getName(), authRole.getId());
            userRoleRepository.save(UserRole.builder()
                .roleId(authRole.getId())
                .username(user.getUsername())
                .build());
            return HttpStatus.OK;
        } else {
            throw new RuntimeException("SMS CODE NOT VALID");
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.DELETE)
    public void logoutAndClean() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        loginService.removeAllAuthRolesForUser(auth.getName());
    }
}
