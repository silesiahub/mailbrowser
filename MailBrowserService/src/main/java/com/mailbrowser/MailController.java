package com.mailbrowser;

import com.mailbrowser.domain.OlderRequestData;
import com.mailbrowser.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class MailController {

    @Autowired
    private MailService mailService;

    @RequestMapping(value = "api/archiveMail", method = RequestMethod.POST)
    @ResponseBody
    public HttpStatus sms(@RequestBody OlderRequestData olderRequestData) {
        if (mailService.sendRequest(olderRequestData)) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
