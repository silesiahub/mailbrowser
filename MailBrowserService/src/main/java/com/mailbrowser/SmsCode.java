package com.mailbrowser;

import lombok.Data;

@Data
public class SmsCode {
    private String smsCode;
}
