package com.mailbrowser.domain;

import lombok.Data;

@Data
public class OlderRequestData {
    private String requestedMailAddress;
    private String fromDate;
    private String toDate;
    private String additionalInfo;
    private String userMail;
    private String username;
    private String userPhoneNumber;
}
