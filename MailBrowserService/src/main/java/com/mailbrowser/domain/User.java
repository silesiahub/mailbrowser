package com.mailbrowser.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NoArgsConstructor
@ToString
@Data
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private UUID integration_key;
    private String username;
    private String phoneNumber;
    private String email;
    private String password;
    private String createdBy;
    //private LocalDate creationDate;
    private String modifiedBy;
    //private LocalDate modificationDate;
    private Integer enabled;
    private String smsCode;
    @Column(name = "valid_to", columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;

}
