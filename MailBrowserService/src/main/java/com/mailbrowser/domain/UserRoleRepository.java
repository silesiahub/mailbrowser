package com.mailbrowser.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
    UserRole save(UserRole userRole);
    List<UserRole> findByUsername(String username);
    void delete(UserRole userRole);
}
