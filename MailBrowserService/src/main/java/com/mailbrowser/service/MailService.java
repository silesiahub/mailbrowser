package com.mailbrowser.service;

import com.mailbrowser.domain.OlderRequestData;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service
public class MailService {

    public boolean sendRequest(OlderRequestData olderRequestData) {
        final String username = "mailbrowserarchiver@gmail.com";
        final String password = "_#MailBrowser#_";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
            "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("mailbrowserarchiver@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("emailbrowser@silesiahub.com"));
            message.setSubject(MessageFormat.format("Older Mail Request - {0}", olderRequestData.getRequestedMailAddress()));
            message.setContent(MessageFormat.format("Requested mail : {0} <br/>" +
                    "From: {1} <br/>" +
                    "To: {2} <br/>" +
                    "Additional info: {3} <br/>" +
                    "Requesting user: {4} <br/>" +
                    "Requesting user mail: {5} <br/>" +
                    "Requesting user phone number: {6} <br/>",
                olderRequestData.getRequestedMailAddress(),
                olderRequestData.getFromDate(),
                olderRequestData.getToDate(),
                olderRequestData.getAdditionalInfo(),
                olderRequestData.getUsername(),
                olderRequestData.getUserMail(),
                olderRequestData.getUserPhoneNumber()), "text/html");

            Transport.send(message);
            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
        return true;
    }
}
