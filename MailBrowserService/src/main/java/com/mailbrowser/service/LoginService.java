package com.mailbrowser.service;

import com.mailbrowser.domain.RoleRepository;
import com.mailbrowser.domain.UserRepository;
import com.mailbrowser.domain.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class LoginService {
    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;


    public void removeRoleForUser(String username, Integer roleId) {//todo: move to some service
        userRoleRepository.findByUsername(username)
            .stream().filter(userRole -> userRole.getRoleId().equals(roleId))
            .forEach(userRoleRepository::delete);
    }

    public void removeAllAuthRolesForUser(String username) {
        Arrays.asList(roleRepository.findByRole("auth"), roleRepository.findByRole("preauth")).stream()
            .map(authRoles -> authRoles.getId())
            .forEach(authRoleId -> removeRoleForUser(username, authRoleId));
    }
}
