package com.mailbrowser.mailreader;

import com.mailbrowser.beans.MailFolder;
import com.mailbrowser.beans.MailMessage;
import com.mailbrowser.beans.MailMessageSimple;
import com.pff.PSTAttachment;
import com.pff.PSTException;
import com.pff.PSTFile;
import com.pff.PSTFolder;
import com.pff.PSTMessage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.mailbrowser.beans.MailFolder;
import com.mailbrowser.beans.MailMessage;
import com.mailbrowser.beans.MailMessageSimple;
import com.pff.PSTAttachment;
import com.pff.PSTException;
import com.pff.PSTFile;
import com.pff.PSTFolder;
import com.pff.PSTMessage;

@Service
@PropertySource("classpath:environment.properties")
@Scope(value = "application", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PstReader {

	final static Logger logger = LoggerFactory.getLogger(PstReader.class);

    private String currentPstFileName;

    @Value("${pst.files.location}")
    private String pstLocation;

	public MailFolder readPstFileStructure(String fileName) {
		this.currentPstFileName = fileName;
		PSTFile pstFile;
		try {
			pstFile = getPstFile(fileName);
			System.out.println(pstFile.getMessageStore().getDisplayName());
			PSTFolder pstFolder = pstFile.getRootFolder();
			MailFolder folder = new MailFolder(pstFolder);
			getSubfolders(folder, pstFolder);
			return folder;
		} catch (PSTException | IOException e) {
			logger.error(
			e.getMessage(), e);
		}
		return null;
	}

    public void getSubfolders(MailFolder folder, PSTFolder pstFolder) {
        try {
            Vector<PSTFolder> pstSubfolders = pstFolder.getSubFolders();
            for (PSTFolder pstSubfolder : pstSubfolders) {

				MailFolder mailSubfolder = new MailFolder(pstSubfolder);
				folder.getSubfolders().add(mailSubfolder);
				getSubfolders(mailSubfolder, pstSubfolder);
			}
		} catch (PSTException e) {
			logger.error(
			e.getMessage(), e);
		} catch (IOException e) {
			logger.error(
			e.getMessage(), e);
		}

    }

	public List<MailMessageSimple> listMessages(long nodeId) {
		List<MailMessageSimple> messages = new ArrayList<>();
		PSTFile pstFile;
		try {
			pstFile = getPstFile(currentPstFileName);
			PSTFolder pstFolder = (PSTFolder) PSTFolder.detectAndLoadPSTObject(pstFile, nodeId);
			if (pstFolder.getContentCount() > 0) {
				PSTMessage email = (PSTMessage) pstFolder.getNextChild();
				while (email != null) {
					messages.add(new MailMessageSimple(email));
					email = (PSTMessage) pstFolder.getNextChild();
				}
			}
			return messages;
		} catch (FileNotFoundException e) {
			logger.error(
			e.getMessage(), e);
		} catch (PSTException e) {
			logger.error(
			e.getMessage(), e);
		} catch (IOException e) {
			logger.error(
			e.getMessage(), e);
		}
		return Collections.EMPTY_LIST;
	}

    public MailMessage getMessageDetails(long nodeId) {
        PSTFile pstFile;
        try {
            pstFile = getPstFile(currentPstFileName);
            PSTMessage pstMessage = (PSTMessage) PSTFolder.detectAndLoadPSTObject(pstFile, nodeId);
            MailMessage message = new MailMessage(pstMessage, true);

            if (pstMessage != null) {
                int numberOfAttachments = pstMessage.getNumberOfAttachments();
                if (numberOfAttachments > 0) {
                    for (int x = 0; x < numberOfAttachments; x++) {
                        PSTAttachment attach = pstMessage.getAttachment(x);
                        String attachmentName = attach.getLongFilename();
                        if (message.getAttachments() == null) {
                            message.setAttachments(new ArrayList<>());
                        }
                        message.getAttachments().add(attachmentName);
                    }
                }
            }

			return message;
		} catch (FileNotFoundException e) {
			logger.error(
			e.getMessage(), e);
		} catch (PSTException e) {
			logger.error(
			e.getMessage(), e);
		} catch (IOException e) {
			logger.error(
			e.getMessage(), e);
		}
		return null;
	}

    private PSTFile getPstFile(String fileName) throws FileNotFoundException, PSTException, IOException {
        PSTFile pstFile;
        File foundPstFile = FileUtils.listFiles(new File(pstLocation), new NameFileFilter(fileName), DirectoryFileFilter.DIRECTORY)
            .stream().findFirst().orElse(null);
        pstFile = new PSTFile(foundPstFile);
        return pstFile;
    }

	public Map.Entry<String,File> downloadAttachment(long nodeId, int index) {
		PSTAttachment attach;
		try {
			PSTMessage email = (PSTMessage) PSTFolder.detectAndLoadPSTObject(getPstFile(currentPstFileName), nodeId);
			attach = email.getAttachment(index);
			InputStream attachmentStream = attach.getFileInputStream();
			// both long and short filenames can be used for attachments
			String filename = attach.getLongFilename();
			if (filename.isEmpty()) {
				filename = attach.getFilename();
			}
			String[] fileNameParts = filename.split("\\.");File file =  File.createTempFile(fileNameParts[0], fileNameParts[1]);
			file.deleteOnExit();
			FileOutputStream out = new FileOutputStream(file);
			// 8176 is the block size used internally and should give the best performance
			int bufferSize = 8176;
			byte[] buffer = new byte[bufferSize];
			int count = attachmentStream.read(buffer);
			while (count == bufferSize) {
				out.write(buffer);
				count = attachmentStream.read(buffer);
			}
			byte[] endBuffer = new byte[count];
			System.arraycopy(buffer, 0, endBuffer, 0, count);
			out.write(endBuffer);
			out.close();
			attachmentStream.close();
			return new AbstractMap.SimpleEntry<String, File>(filename,file);
		} catch (PSTException e) {
			logger.error(
			e.getMessage(), e);
		} catch (IOException e) {
			logger.error(
			e.getMessage(), e);
		}
		return null;
	}

}
