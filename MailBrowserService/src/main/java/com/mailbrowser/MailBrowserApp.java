package com.mailbrowser;

import com.mailbrowser.beans.MailFolder;
import com.mailbrowser.beans.MailMessage;
import com.mailbrowser.beans.MailMessageSimple;
import com.mailbrowser.mailreader.PstReader;
import liquibase.integration.spring.SpringLiquibase;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.sql.DataSource;

@SpringBootApplication(exclude = org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class)
@PropertySource("classpath:environment.properties")
@RestController
public class MailBrowserApp extends SpringBootServletInitializer {

    @Autowired
    private @Setter
    PstReader pstReader;

    @Value("${pst.files.location}")
    private String pstLocation;

    @Bean
    @ConfigurationProperties(prefix = "db.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog("classpath:/db/db.changelog-master.xml");
        liquibase.setDataSource(dataSource());
        return liquibase;
    }

    @RequestMapping("/api/structure")
    public Map<String, Object> home() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("id", UUID.randomUUID().toString());
        model.put("content", "Hello World");
        model.put("folders", getFiles());
        return model;
    }

    @RequestMapping("/api/list/{pstFileName:.+}")
    public Map<String, Object> listPst(@PathVariable("pstFileName") String pstFileName) {
        Map<String, Object> model = new HashMap<String, Object>();
        MailFolder mainFolder = pstReader.readPstFileStructure(pstFileName);
        model.put("pstFolder", mainFolder);
        return model;
    }

    @RequestMapping("/api/listMessages/{nodeId:.+}")
    public Map<String, Object> listEmails(@PathVariable("nodeId") long nodeId) {
        Map<String, Object> model = new HashMap<String, Object>();
        List<MailMessageSimple> messages = pstReader.listMessages(nodeId);
        model.put("emails", messages);
        return model;
    }

    @RequestMapping("/api/messageDetails/{nodeId:.+}")
    public Map<String, Object> messageDetails(@PathVariable("nodeId") long nodeId) {
        Map<String, Object> model = new HashMap<String, Object>();
        MailMessage message = pstReader.getMessageDetails(nodeId);
        model.put("message", message);
        return model;
    }

//	@RequestMapping("/attachment/{nodeId}/{index}")
//	  public Map<String,Object> getAttachment(@PathVariable("nodeId") long nodeId, @PathVariable("index") int index) {
//	    Map<String,Object> model = new HashMap<String,Object>();
//	    MailMessage message = pstReader.downloadAttachment(nodeId, index);
//	    model.put("message", message);
//	    return model;
//	  }

    @RequestMapping(value = "/api/attachment/{nodeId}/{index}", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> downloadStuff(@PathVariable("nodeId") long nodeId, @PathVariable("index") int index)
        throws IOException {
        Map.Entry<String, File> file = pstReader.downloadAttachment(nodeId, index);

        HttpHeaders respHeaders = new HttpHeaders();
        String[] nameParts = file.getValue().getName().split("\\.");
        String type = nameParts[nameParts.length - 1];
        respHeaders.setContentType(MediaType.IMAGE_PNG);
        respHeaders.setContentLength(file.getValue().length());
        respHeaders.setContentDispositionFormData("attachment", file.getKey());

        InputStreamResource isr = new InputStreamResource(new FileInputStream(file.getValue()));
        return new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
    }

    public List<String> getFiles() {
//        ClassLoader classLoader = getClass().getClassLoader();
//        String path = classLoader.getResource("files").getPath();
        File directory = new File(pstLocation);

        List<String> resultList = new ArrayList<String>();

        // get all the files from a directory
        FileUtils.listFiles(directory,
            new RegexFileFilter(".*(.pst)"),
            DirectoryFileFilter.DIRECTORY)
            .forEach(file -> resultList.add(file.getName()));
//        for (int i = 0; i < fList.length; i++) {
//            resultList.add(fList[i].getName());
//        }
//        resultList.addAll(Arrays.asList(fList));
//        for (File file : fList) {
//            if (file.isFile()) {
//                System.out.println(file.getAbsolutePath());
//            } else if (file.isDirectory()) {
//                resultList.addAll((file.getAbsolutePath());
//            }
//        }
//        //System.out.println(fList);
        return resultList;
    }


    public static void main(String[] args) {
        SpringApplication.run(MailBrowserApp.class, args);
    }
}
