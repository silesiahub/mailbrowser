--liquibase formatted sql

--changeset jmachnik:db.changelog.001.1

---------------------------------------------------------------------------------------------
-- USER table
---------------------------------------------------------------------------------------------

CREATE TABLE mailbrowser.users
(
  id                SERIAL PRIMARY KEY,
  integration_key   UUID                     NOT NULL,
  username          TEXT                     NOT NULL DEFAULT '' :: TEXT,
  phone_number      TEXT                     NOT NULL DEFAULT '' :: TEXT,
  email             TEXT                     NOT NULL DEFAULT '' :: TEXT,
  password          TEXT                     NOT NULL DEFAULT '',
  created_by        TEXT                     NOT NULL,
  creation_date     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  modified_by       TEXT                     NOT NULL,
  modification_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  status            TEXT                     NOT NULL
)
WITH (
OIDS = FALSE
);

CREATE UNIQUE INDEX idx_user_username
  ON mailbrowser.users
  USING BTREE
  (username);

CREATE UNIQUE INDEX idx_user_email
  ON mailbrowser.users
  USING BTREE
  (email);


CREATE TABLE user_roles (
  user_role_id SERIAL PRIMARY KEY,
  username     VARCHAR(45) NOT NULL REFERENCES users (username),
  role         VARCHAR(45) NOT NULL
) WITH (
OIDS = FALSE
);

ALTER TABLE users DROP COLUMN status;
ALTER TABLE users ADD COLUMN enabled INTEGER NOT NULL DEFAULT(1);


--changeset jmachnik:db.changelog.001.2

CREATE TABLE roles (
  id serial PRIMARY KEY,
  role  VARCHAR(45)
) WITH (
  OIDS = false
);

ALTER TABLE user_roles DROP COLUMN role;
ALTER TABLE user_roles ADD COLUMN role_id INTEGER REFERENCES roles(id);

--changeset jmachnik:db.changelog.001.3

ALTER TABLE users ADD COLUMN sms_code VARCHAR(45);
ALTER TABLE users ADD COLUMN valid_to TIMESTAMP WITH TIME ZONE;